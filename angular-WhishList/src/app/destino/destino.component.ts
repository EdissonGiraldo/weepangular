import { Component, OnInit } from '@angular/core';
import { DestinoViaje} from './../models/destino-viaje.model';

@Component({
  selector: 'app-destino',
  templateUrl: './destino.component.html',
  styleUrls: ['./destino.component.css']
})
export class DestinoComponent implements OnInit {
 destinos: DestinoViaje[];

  constructor() {
  	this.destinos= [];
  }

  ngOnInit(): void {
  }
  guardar(nombre:string, url:string):boolean{
  	this.destinos.push(new DestinoViaje(nombre, url));
  	console.log(this.destinos);
  	return false;

  }

}
